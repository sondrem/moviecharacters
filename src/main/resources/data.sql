INSERT INTO character(name, alias, gender) VALUES ('Spiderman', 'Peter Parker', 'Male');
INSERT INTO character(name, alias, gender) VALUES ('Captain America', 'Steve Rogers', 'Male');
INSERT INTO character(name, alias, gender) VALUES ('Captain Marvel', 'Carol Denvers', 'Female');
INSERT INTO character(name, alias, gender) VALUES ('Gollum', 'Smeagol', 'Male');
INSERT INTO character(name, alias, gender) VALUES ('Aragorn', 'Elessar', 'Male');

INSERT INTO franchise(name, description) VALUES ('Lord of The Rings', 'One ring to rule them all');
INSERT INTO franchise(name, description) VALUES ('Marvel Cinematic Universe', 'Bunch of heroes trying to save the world');

INSERT INTO movie(title, genre, release_year, director, franchise_id) VALUES ('Endgame', 'Action', '2019', 'Anthony Russo', 2);
INSERT INTO movie(title, genre, release_Year, director, franchise_id) VALUES ('The First Avenger', 'Action', '2011', 'Joe Johnston', 2);
INSERT INTO movie(title, genre, release_year, director, franchise_id) VALUES ('Captain Marvel', 'Action', '2019', 'Anna Boden', 2);
INSERT INTO movie(title, genre, release_year, director, franchise_id) VALUES ('Fellowship of the Ring', 'Fantasy', '2001', 'Peter Jackson', 1);


INSERT INTO movie_characters(characters_id, movies_id) VALUES (1, 1);
INSERT INTO movie_characters(characters_id, movies_id) VALUES (2, 2);
INSERT INTO movie_characters(characters_id, movies_id) VALUES (2, 1);
INSERT INTO movie_characters(characters_id, movies_id) VALUES (3, 3);
INSERT INTO movie_characters(characters_id, movies_id) VALUES (3, 1);
INSERT INTO movie_characters(characters_id, movies_id) VALUES (4, 4);
INSERT INTO movie_characters(characters_id, movies_id) VALUES (5, 4);


