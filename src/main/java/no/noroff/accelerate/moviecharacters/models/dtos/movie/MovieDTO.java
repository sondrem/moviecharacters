package no.noroff.accelerate.moviecharacters.models.dtos.movie;

import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String picture;
    private String trailer;
    private Set<Integer> characters;
    private int franchise;
}
