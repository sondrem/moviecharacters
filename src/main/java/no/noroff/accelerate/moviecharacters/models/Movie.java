package no.noroff.accelerate.moviecharacters.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 30, nullable = false)
    private String title;
    @Column(length = 30, nullable = false)
    private String genre;
    @Column(nullable = false)
    private int releaseYear;
    @Column(length = 30, nullable = false)
    private String director;
    private String picture;
    private String trailer;

    // Relations
    @ManyToMany
    private Set<Character> characters;
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
