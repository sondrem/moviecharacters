package no.noroff.accelerate.moviecharacters.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;

    //Relations
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
