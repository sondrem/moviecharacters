package no.noroff.accelerate.moviecharacters.configs;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("prod")
public class ProdDataSource {
    @Bean
    public DataSource getDevDataSource() {
        DataSourceBuilder builder = DataSourceBuilder.create();
        builder.driverClassName("org.postgresql.Driver");
        builder.url("jdbc:postgresql://ec2-52-212-228-71.eu-west-1.compute.amazonaws.com:5432/d14nlomr9o00en");
        builder.username("fxtwjqxnyxlucy");
        builder.password("3c8ecb0a3057f21e09f3a9804592205a8e97f8a3616219cbb1a55c4f33d9a949");
        return builder.build();
    }
}
