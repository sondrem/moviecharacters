package no.noroff.accelerate.moviecharacters.repositories;

import no.noroff.accelerate.moviecharacters.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository (DAO) for the Character domain class.
 */
@Repository
public interface CharacterRepository extends JpaRepository <Character,Integer> {
}
