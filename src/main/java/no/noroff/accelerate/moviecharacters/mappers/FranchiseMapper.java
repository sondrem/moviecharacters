package no.noroff.accelerate.moviecharacters.mappers;

import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.models.dtos.franchise.FranchiseDTO;
import no.noroff.accelerate.moviecharacters.models.dtos.movie.MovieDTO;
import no.noroff.accelerate.moviecharacters.services.character.CharacterService;
import no.noroff.accelerate.moviecharacters.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {

    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchisesToFranchisesDTO(Collection<Franchise> franchise);

    @Mapping(target = "movies", ignore = true)
    public abstract Franchise franchiseDTOToFranchise(FranchiseDTO dto);

    //Custom mappings
    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(Movie::getId).collect(Collectors.toSet());
    }
}
