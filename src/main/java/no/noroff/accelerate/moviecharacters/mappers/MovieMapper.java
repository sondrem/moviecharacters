package no.noroff.accelerate.moviecharacters.mappers;

import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.models.dtos.movie.MovieDTO;
import no.noroff.accelerate.moviecharacters.services.character.CharacterService;
import no.noroff.accelerate.moviecharacters.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected CharacterService characterService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDTO(Movie movie);

    public abstract Collection<MovieDTO> moviesToMoviesDto(Collection<Movie> movies);

    @Mapping(target = "franchise", ignore = true)
    @Mapping(target = "characters", ignore = true)
    public abstract Movie movieDTOToMovie(MovieDTO dto);

    //Custom mappings
    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<Character> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(Character::getId).collect(Collectors.toSet());
    }
}
