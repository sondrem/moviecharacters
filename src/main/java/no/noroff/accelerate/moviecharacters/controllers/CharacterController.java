package no.noroff.accelerate.moviecharacters.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.moviecharacters.mappers.CharacterMapper;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.dtos.character.CharacterDTO;
import no.noroff.accelerate.moviecharacters.models.dtos.movie.MovieDTO;
import no.noroff.accelerate.moviecharacters.services.character.CharacterService;
import no.noroff.accelerate.moviecharacters.utils.ApiErrorResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get a character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("{id}") // GET: localhost:8080/api/v1/characters/{id}
    public ResponseEntity getById(@PathVariable int id) {
        Character character = characterService.findById(id);
        CharacterDTO characterDTO = characterMapper.characterToCharacterDTO(character);
        return ResponseEntity.ok(characterDTO);
    }

    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping // GET: localhost:8080/api/v1/characters
    public ResponseEntity getAll() {
        Collection<CharacterDTO> characterDTOs = characterMapper.charactersToCharactersDTO(
                characterService.findAll()
        );
        return ResponseEntity.ok(characterDTOs);
    }

    @Operation(summary = "Add new character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
    })
    @PostMapping // POST: localhost:8080/api/v1/characters
    public ResponseEntity add(@RequestBody CharacterDTO characterDTO) {
        Character character = characterMapper.characterDTOToCharacter(characterDTO);
        characterService.add(character);
        URI location = URI.create("characters/" + character.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a character by ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") // PUT: localhost:8080/api/v1/character/{id}
    public ResponseEntity update(@RequestBody CharacterDTO characterDTO, @PathVariable int id) {
        // Validates if body is correct
        if(id != characterDTO.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(
                characterMapper.characterDTOToCharacter(characterDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/characters/{id}
    public ResponseEntity delete(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
