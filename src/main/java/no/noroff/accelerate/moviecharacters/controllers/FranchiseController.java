package no.noroff.accelerate.moviecharacters.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.moviecharacters.mappers.CharacterMapper;
import no.noroff.accelerate.moviecharacters.mappers.FranchiseMapper;
import no.noroff.accelerate.moviecharacters.mappers.MovieMapper;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.models.dtos.character.CharacterDTO;
import no.noroff.accelerate.moviecharacters.models.dtos.franchise.FranchiseDTO;
import no.noroff.accelerate.moviecharacters.models.dtos.movie.MovieDTO;
import no.noroff.accelerate.moviecharacters.services.franchise.FranchiseService;
import no.noroff.accelerate.moviecharacters.utils.ApiErrorResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    public FranchiseController(FranchiseService franchiseService,
                               FranchiseMapper franchiseMapper,
                               MovieMapper movieMapper,
                               CharacterMapper characterMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{id}") // GET: localhost:8080/api/v1/franchises/{id}
    public ResponseEntity getById(@PathVariable int id) {
        return ResponseEntity.ok(franchiseMapper
                .franchiseToFranchiseDTO(franchiseService.findById(id)));
    }

    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping // GET: localhost:8080/api/v1/franchises
    public ResponseEntity getAll() {
        Collection<FranchiseDTO> franchises = franchiseMapper.franchisesToFranchisesDTO(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchises);
    }

    @Operation(summary = "Add new franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
    })
    @PostMapping // POST: localhost:8080/api/v1/franchises
    public ResponseEntity add(@RequestBody FranchiseDTO franchiseDTO) {
        Franchise fra = franchiseMapper.franchiseDTOToFranchise((franchiseDTO));
        franchiseService.add(fra);
        URI location = URI.create("franchises/" + fra.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") // PUT: localhost:8080/api/v1/franchises/{id}
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id) {
        // Validates if body is correct
        if (id != franchiseDTO.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(
                franchiseMapper.franchiseDTOToFranchise(franchiseDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update movies in franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}/movies") // PUT: localhost:8080/api/v1/franchises/{id}/movies
    public ResponseEntity updateMoviesInFranchise(@RequestBody int[] movies, @PathVariable int id) {
        franchiseService.updateMoviesInFranchise(movies, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all movies in franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("{id}/movies") // GET: localhost:8080/api/v1/franchises/{id}/movies
    public ResponseEntity getMoviesInFranchise(@PathVariable int id) {
        Collection<Movie> movies = franchiseService.getMoviesInFranchise(id);
        Collection<MovieDTO> movieDTOS = movieMapper.moviesToMoviesDto(movies);
        return ResponseEntity.ok(movieDTOS);
    }

    @Operation(summary = "Get all characters in franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("{id}/characters") // GET: localhost:8080/api/v1/franchises/{id}/characters
    public ResponseEntity getCharactersInFranchise(@PathVariable int id) {
        Collection<Character> characters = franchiseService.getCharactersInFranchise(id);
        Collection<CharacterDTO> characterDTOS = characterMapper.charactersToCharactersDTO(characters);
        return ResponseEntity.ok(characterDTOS);
    }

    @Operation(summary = "Delete a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/franchises/{id}
    public ResponseEntity delete(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
