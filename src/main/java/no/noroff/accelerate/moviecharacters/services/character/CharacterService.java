package no.noroff.accelerate.moviecharacters.services.character;


import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.services.CrudService;


/**
 * Service for the Character domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface CharacterService extends CrudService<Character,Integer> {
}
