package no.noroff.accelerate.moviecharacters.services.franchise;

import no.noroff.accelerate.moviecharacters.exceptions.FranchiseNotFoundException;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.repositories.FranchiseRepository;
import no.noroff.accelerate.moviecharacters.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the Franchise service.
 * Uses the Franchise repository to interact with the data store.
 */
@Service
public class FranchiseServiceImpl implements FranchiseService{
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(()->new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if(franchiseRepository.existsById(id)) {
            // Set relationships to null so we can delete without referential problems
            Franchise franchise = franchiseRepository.findById(id).get();
            franchise.getMovies().forEach(s -> s.setFranchise(null));
            franchiseRepository.delete(franchise);
        }
    }

    @Override
    public boolean exists(Integer id) {
        return franchiseRepository.existsById(id);
    }

    @Override
    public void updateMoviesInFranchise(int[] movies, int franchiseId) {
        Franchise franchise = franchiseRepository.findById(franchiseId)
                .orElseThrow(() ->new FranchiseNotFoundException(franchiseId));

        //remove current movies in franchise
        Set<Movie> movieList = franchise.getMovies();
        for (Movie m: movieList) {
            m.setFranchise(null);
            movieRepository.save(m);
        }

        //set franchise for updated movies
        for (int j : movies) {
            Movie movie = movieRepository.findById(j).get();
            movie.setFranchise(franchise);
            movieRepository.save(movie);
        }

        franchiseRepository.save(franchise);
    }

    @Override
    public Collection<Movie> getMoviesInFranchise(int franchiseId) {
        Franchise franchise = franchiseRepository.findById(franchiseId)
                .orElseThrow(() ->new FranchiseNotFoundException(franchiseId));
        return franchise.getMovies();
    }

    @Override
    public Collection<Character> getCharactersInFranchise(int franchiseId) {
        Franchise franchise = franchiseRepository.findById(franchiseId)
                .orElseThrow(() ->new FranchiseNotFoundException(franchiseId));
        Collection<Movie> movies = franchise.getMovies();
        Collection<Character> characters = new HashSet<>();
        for (Movie movie: movies)
            characters.addAll(movie.getCharacters());
        return characters;
    }
}
