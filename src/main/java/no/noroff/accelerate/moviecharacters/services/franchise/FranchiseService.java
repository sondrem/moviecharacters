package no.noroff.accelerate.moviecharacters.services.franchise;

import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.services.CrudService;

import java.util.Collection;

/**
 * Service for the Franchise domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface FranchiseService extends CrudService<Franchise,Integer> {

    /**
     * This function updates the franchise with a list of given movies
     * @param movies new list of movies to be added to franchise
     * @param franchiseId ID of the franchise to be updated with movies
     */
    void updateMoviesInFranchise(int[] movies, int franchiseId);

    /**
     * This method gets all the movies in a franchise
     * @param franchiseId franchise to get movies from
     * @return collection of movies to be sent through API
     */
    Collection<Movie> getMoviesInFranchise(int franchiseId);


    /**
     * This method gets all the characters in a franchise
     * @param franchiseId franchise to get characters from
     * @return collection of characters to be sent through API
     */
    Collection<Character> getCharactersInFranchise(int franchiseId);
}
