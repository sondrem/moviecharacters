package no.noroff.accelerate.moviecharacters.services.character;

import no.noroff.accelerate.moviecharacters.exceptions.CharacterNotFoundException;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

/**
 * Implementation of the Character service.
 * Uses the Character repository to interact with the data store.
 */
@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(()->new CharacterNotFoundException(id));
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if(characterRepository.existsById(id)) {
            // Set relationships to null so we can delete without referential problems
            Character character = characterRepository.findById(id).get();
            character.getMovies().forEach(s -> s.getCharacters().remove(character));
            characterRepository.delete(character);
        }
    }

    @Override
    public boolean exists(Integer id) {
        return characterRepository.existsById(id);
    }
}
