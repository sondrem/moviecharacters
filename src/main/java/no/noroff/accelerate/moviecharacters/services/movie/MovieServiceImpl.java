package no.noroff.accelerate.moviecharacters.services.movie;

import no.noroff.accelerate.moviecharacters.exceptions.MovieNotFoundException;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.repositories.CharacterRepository;
import no.noroff.accelerate.moviecharacters.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the Movie service.
 * Uses the Movie repository to interact with the data store.
 */
@Service
public class MovieServiceImpl implements MovieService{
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(()->new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if(movieRepository.existsById(id)) {
            // Set relationships to null so we can delete without referential problems
            Movie movie = movieRepository.findById(id).get();
            movie.getCharacters().forEach(s -> s.getMovies().remove(movie));
            movieRepository.delete(movie);
        }
    }

    @Override
    public boolean exists(Integer id) {
        return movieRepository.existsById(id);
    }

    @Override
    public void updateCharactersInMovie(int[] characters, int movieId) {
        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(()->new MovieNotFoundException(movieId));
        Set<Character> characterList = new HashSet<>();
        for (int j : characters) {
            Character character = characterRepository.findById(j).get();
            characterList.add(character);
        }
        movie.setCharacters(characterList);
        movieRepository.save(movie);
    }

    @Override
    public Collection<Character> getCharactersInMovie(int movieId) {
        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(()->new MovieNotFoundException(movieId));
        return movie.getCharacters();
    }
}
