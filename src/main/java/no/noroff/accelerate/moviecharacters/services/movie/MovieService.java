package no.noroff.accelerate.moviecharacters.services.movie;

import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.services.CrudService;

import java.util.Collection;

/**
 * Service for the Movie domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface MovieService extends CrudService<Movie,Integer> {

    /**
     * This function updates the movie with a list of given characters
     * @param characters new list of characters to be added to movie
     * @param movieId ID of the movie to be updated with characters
     */
    void updateCharactersInMovie(int[] characters, int movieId);


    /**
     * This method gets all the characters in a movie
     * @param movieId movie to get characters from
     * @return collection of characters to be sent through API
     */
    Collection<Character> getCharactersInMovie(int movieId);
}
