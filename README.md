# Web API and database with Spring

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/NicholasLennox/gradle-ci/badges/master/pipeline.svg)](https://gitlab.com/sondrem/moviecharacters/-/commits/master)

PostgreSQL database using Hibernate and Spring Web to create a Web API.

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Integration](#Integration)
- [Built with](#built-with)
- [Contributing](#contributing)
- [License](#license)

## About
This project is about creating an application with a datastore and interface to store and manipulate
movie characters. The application is constructed in Spring Web and comprise of a database made 
in PostgreSQL through Hibernate with a RESTful API to allow users to manipulate the data. The database 
is storing information about characters, movies they appear in, and the franchises these movies belong to.

### Appendix A: Use Hibernate to create a database
Creating a database to store information about characters, movies they appear in, 
and the franchises these movies belong to. The following rules apply: 
- One movie contains many characters, and a character can play in multiple movies. 
- One movie belongs to one franchise, but a franchise can contain many movies. 

We have Repositories (extending JPA Repository) to encapsulate data access, and Services to
encapsulate any business logic. We are also adding some dummy data to be stored. 

### Appendix B: Create a Web API in Spring Web
Creating dedicated endpoints for the system on a high level. We have developed full
CRUD-functionality for movies, characters and franchises. When new resources are added, 
the application do not add related data at the same time. We have two dedicated endpoints
to be used for this matter: 

- Updating characters in a movie
- Updating movies in a franchise

We also have dedicated endpoints for the following functionality: 

- Get all the movies in a franchise. 
- Get all the characters in a movie. 
- Get all the characters in a franchise.

Data Transfer Objects (DTOs) are used for information transfer, and Mapstruct is used 
to map between domain entities and DTOs. 

Proper API-documentation is created using Swagger/Open API.

## Install
Clone repository and build project.

## Usage
Run MovieCharactersApplication.java and use dedicated endpoints to 
interact with the application. 

## Integration
There exists a CI/CD pipeline to build out application as a Docker artifact. There is also
a second job to deploy the application to Heroku with a manual trigger.

## Built with

- Java JDK 17
- Spring Web
- Spring Data JPA
- Lombok
- SQL
- PostGres
- PGAdmin
- Postgres SQL driver dependency
- Gradle - Dependency Management
- Docker

## Contributing
- [Lars-Inge Gammelsæter Jonsen](https://gitlab.com/Kaladinge)
- [Sondre Mæhre](https://gitlab.com/sondrem)

PRs accepted.

## License

UNLICENSED
